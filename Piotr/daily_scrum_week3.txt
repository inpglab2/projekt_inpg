2018-05-21

Dodanie dzia�aj�cego zegara do strony g��wnej.
"Podlinkowanie" odno�nik�w do fb, twittera, yt i g+.
Opracowanie i umieszczenie na stronie g��wnej logo sklepu.
Dodanie do strony g��wnej przycisk�w logowania i rejestracji.
Zmiana tre�ci strony g��wnej z tymczasowej na w�a�ciw�.
Poprawa wizualna zawarto�ci strony g��wnej.

Brak napotkanych problem�w.
_________________
2018-05-22

Utworzenie podstrony s�u��cej do logowania. 
Utworzenie korelacji strony g��wnej z ww. (po klikni�ciu odpowiedniego przycisku jeste�my przekierowani).
Zmiana monochromatycznego t�a na �agodny wz�r czyni�cy przegl�danie sklepu przyjemnym.

Brak napotkanych problem�w.
_________________
2018-05-23

Utworzenie podstrony do rejestracji nowych u�ytkownik�w.
Utworzenie podstrony s�u��cej do resetowania loginu i has�a.
Praca nad nawigacj� po stronie - na ww. podstronach znajduje si� odno�nik do strony g��wnej oraz logo marki. 

Brak napotkanych problem�w podczas wykonywania zadania.
_________________
2018-05-24

Nauka o tworzeniu podstron, kt�re b�d� mog�y wy�wietla� elementy z bazy produkt�w.
Stworzenie szablonu strony s�u��cej do wy�wietlania produkt�w z bazy danych/katalogu.
Podlinkowanie powy�szej strony ze strony g��wnej.

Bezproblemowo wykonane zadanie.
_________________
2018-05-25

Dopracowywanie podstrony z baz� produkt�w, ustawienie kolor�w sp�jnych z og�lnym wygl�dem serwisu. Refleksje na temat informacji, kt�re powinny si� znale�� w opisie produktu oraz przyst�pnym sposobie ich przedstawienia.
Wyszukiwanie w internecie sposob�w na automatyczne wy�wietlanie produkt�w z bazy itp.
Nauka bardziej zaawansowanych opcji, kt�re udost�pnia j�zyk css na podstawie strony https://www.w3schools.com/css/ .

Brak problem�w podczas realizacji zadania.